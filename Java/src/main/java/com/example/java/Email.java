package com.example.java;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class Email {

    private static String USER_NAME = "berger.msn";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "Slimer12"; // GMail password
    private static String RECIPIENT = "kyle.msn@hotmail.com";

    public static void main(String[] args) {
        String from = USER_NAME;
        String pass = PASSWORD;
        String[] to = { RECIPIENT }; // list of recipient email addresses
        String subject = "Pixelot RPG for Desktop, iOS, and Android";
        String filename = "assets/pixelotEmail.html";
        ArrayList<String> fileNames = new ArrayList<>();
        fileNames.add("assets/all.gif");
        fileNames.add("assets/battle.gif");
        fileNames.add("assets/cart.gif");
        fileNames.add("assets/ice.gif");
        ArrayList<String> ids = new ArrayList<>();
        ids.add("all");
        ids.add("battle");
        ids.add("cart");
        ids.add("ice");
        try {
            BufferedReader file = new BufferedReader(new FileReader("assets/websiteEmails.txt"));
            String line = file.readLine();
            ArrayList<String>emails = new ArrayList<>();
            ArrayList<String>usedEmails = new ArrayList<>();
            emails.add(line);
            while ((line = file.readLine()) != null) {
                emails.add(line);
                line = null;
            }
            for(int i = 0; i < emails.size(); i++){
                if(!usedEmails.contains(emails.get(i))) {
                    to[0] = emails.get(i);
                    System.out.print(i+". ");
                    sendFromGMail(from, pass, to, subject, filename, fileNames, ids);
                    usedEmails.add(emails.get(i));
                }
            }
        }
        catch (Exception e){
            System.out.println("Could not read emails");
            System.out.println(e);
        }
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String filename, ArrayList<String>fileNames, ArrayList<String> ids) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            try {

                MimeMultipart multipart = new MimeMultipart("related");

                // first part  (the html)
                BodyPart messageBodyPart = new MimeBodyPart();

                BufferedReader file = new BufferedReader(new FileReader(filename));
                String htmlText = file.readLine();
                String line = null;
                while( (line = file.readLine()) != null){
                    htmlText += line;
                    line = null;
                }
                file.close();
                messageBodyPart.setContent(htmlText, "text/html");

                // add it
                multipart.addBodyPart(messageBodyPart);

                // second part (the image)
                messageBodyPart = new MimeBodyPart();
                DataSource fds = new FileDataSource
                        ("assets\\title.png");
                messageBodyPart.setDataHandler(new DataHandler(fds));
                messageBodyPart.setHeader("Content-ID", "<image>");

                // add it
                multipart.addBodyPart(messageBodyPart);
                for(int i = 0; i < fileNames.size(); i++) {
                    // second part (the image)
                    messageBodyPart = new MimeBodyPart();
                    fds = new FileDataSource
                            (fileNames.get(i));
                    messageBodyPart.setDataHandler(new DataHandler(fds));
                    messageBodyPart.setHeader("Content-ID","<"+ids.get(i)+">");

                    // add it
                    multipart.addBodyPart(messageBodyPart);
                }
                // put everything together
                message.setContent(multipart);
                Transport transport = session.getTransport("smtp");
                transport.connect(host, from, pass);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
                System.out.println("Email Sent Succesffully! " + to[0]);
            }
            catch (Exception e){
                System.out.println("Email Failed!");
            }
        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}